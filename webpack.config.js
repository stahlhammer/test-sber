const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: ['./app/index.tsx'],

  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js?[hash]',
    publicPath: '/',
  },

  devtool: 'source-map',

  resolve: {
    modules: ['node_modules'],
    extensions: ['.webpack.js', '.ts', '.tsx', '.js', '.json'],
  },

  plugins: [
    new HtmlWebpackPlugin({      
      template: path.resolve(__dirname, './app/index.html'),
    }),
  ],

  module: {
    rules: [
      { test: /\.tsx?$/, loader: 'ts-loader' },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
      {
        test: /\.png$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },      
    ],
  },

  devServer: {
      port: '5000',
      contentBase: path.join(__dirname, 'src'),
      compress: true,
      hot: true,
  }
};
