import { ICoin, IDrink, IError, ISuccess } from './Main.interface'
import { observable, computed, action, runInAction } from 'mobx'

export type TInjectedMainStore = {
  mainStore: MainStore
}

export class MainStore {
  @observable
  walletCoins: ICoin[] = [
    {
      value: 1,
      amount: 10,
    },
    {
      value: 2,
      amount: 30,
    },
    {
      value: 5,
      amount: 20,
    },
    {
      value: 10,
      amount: 15,
    },
  ]

  @observable
  machineCoins: ICoin[] = [
    {
      value: 1,
      amount: 100,
    },
    {
      value: 2,
      amount: 100,
    },
    {
      value: 5,
      amount: 100,
    },
    {
      value: 10,
      amount: 100,
    },
  ]

  @observable
  inserted: ICoin[] = []

  @observable
  drinks: IDrink[] = [
    {
      name: 'tea',
      price: 13,
      amount: 10,
    },
    {
      name: 'coffee',
      price: 18,
      amount: 20,
    },
    {
      name: 'cappuccino',
      price: 21,
      amount: 20,
    },
    {
      name: 'juice',
      price: 35,
      amount: 15,
    },
  ]

  @observable
  public hasError: boolean = false

  @observable
  public errorMessage: string = ''

  @observable
  public hasPurchased: boolean = false

  @observable
  public successMessage: string = ''

  @observable
  public change: number = 0

  @computed
  public get walletSum() {
    return this.walletCoins.reduce(
      (sum, coin) => sum + coin.amount * coin.value,
      0
    )
  }

  @computed
  public get machineSum() {
    return this.machineCoins.reduce(
      (sum, coin) => sum + coin.amount * coin.value,
      0
    )
  }

  @computed
  public get insertedSum() {
    return this.inserted.reduce(
      (sum, coin) => sum + coin.amount * coin.value,
      0
    )
  }

  @action.bound
  public insertCoin(value: number) {
    const walletCoinsShadow = [...this.walletCoins]
    let insertedShadow = [...this.inserted]

    const walletCoin = walletCoinsShadow.find(coin => coin.value === value)

    if (walletCoin) {
      walletCoin.amount--
    }

    const coinToInsert = insertedShadow.find(coin => coin.value === value)

    if (coinToInsert) {
      coinToInsert.amount++
    } else {
      insertedShadow = [
        ...insertedShadow,
        {
          value,
          amount: 1,
        },
      ]
    }

    this.inserted = insertedShadow
    this.walletCoins = walletCoinsShadow
  }

  @action.bound
  public moveInsertedMoneyToMachine() {
    const insertedShadow = [...this.inserted]
    const machineCoinsShadow = [...this.machineCoins]

    insertedShadow.forEach(coin => {
      const machineCoin = machineCoinsShadow.find(
        mCoin => mCoin.value === coin.value
      )

      if (machineCoin) {
        machineCoin.amount = machineCoin.amount + coin.amount
      }
    })

    this.machineCoins = machineCoinsShadow
    this.inserted = []
  }

  @action.bound
  public updateChange(price: number) {
    this.change = this.change + (this.insertedSum - price)
  }

  @action.bound
  public giveDrink(drinkName: string) {
    const drinksShadow = [...this.drinks]

    const drinkToGive = drinksShadow.find(drink => drink.name === drinkName)
    if (drinkToGive) {
      drinkToGive.amount--
    }

    this.drinks = drinksShadow
  }

  @action.bound
  public giveChange() {
    const machineCoinsShadow = [...this.machineCoins].reverse()
    const walletCoinsShadow = [...this.walletCoins]

    machineCoinsShadow.forEach(coin => {
      if (this.change && this.change >= coin.value) {
        const newAmountDiff = this.calculateNewAmountDiff(coin.value)

        coin.amount -= newAmountDiff

        const walletCoin = walletCoinsShadow.find(
          wCoin => wCoin.value === coin.value
        )

        if (walletCoin) {
          walletCoin.amount += newAmountDiff
        }
      }
    })

    this.machineCoins = machineCoinsShadow.reverse()
    this.walletCoins = walletCoinsShadow
  }

  @action.bound
  private calculateNewAmountDiff(value: number, amount: number = 0) {
    this.change = this.change - value
    const counter = amount + 1

    if (this.change >= value) {
      return this.calculateNewAmountDiff(value, counter)
    } else {
      return counter
    }
  }

  @action.bound
  public dispatchError(error: IError) {
    this.hasError = true
    this.errorMessage = error.message

    setTimeout(() => {
      runInAction(() => {
        this.hasError = false
        this.errorMessage = ''
      })
    }, 3000)
  }

  @action.bound
  public dispatchSuccess(success: ISuccess) {
    this.hasPurchased = true
    this.successMessage = success.message

    setTimeout(() => {
      runInAction(() => {
        this.hasPurchased = false
        this.successMessage = ''
      })
    }, 3000)
  }
}
