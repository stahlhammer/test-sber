export type TDrinkName = 'tea' | 'coffee' | 'cappuccino' | 'juice'

export interface ICoin {
  value: number
  amount: number
}

export interface IDrink {
  price: number
  amount: number
  name: TDrinkName
}

export interface IError {
  message: string
}

export interface ISuccess {
  message: string
}

export interface IImage {
  default: string
}