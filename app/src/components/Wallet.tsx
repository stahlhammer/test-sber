import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { TInjectedMainStore } from '../stores/Main.store'
import { action } from 'mobx'
import { Coin } from './Coin'
import { ICoin } from '../stores/Main.interface'

export interface IWalletProps {}

@inject('mainStore')
@observer
export default class Wallet extends React.Component<IWalletProps> {
  private get injected() {
    return this.props as IWalletProps & TInjectedMainStore
  }

  private get mainStore() {
    return this.injected.mainStore
  }

  @action.bound
  private onClickCoin(value: number) {
    this.mainStore.insertCoin(value)
  }

  @action.bound
  private renderCoins(walletCoins: ICoin[]) {
    return walletCoins.map(coin => {
      return <Coin {...coin} key={coin.value} onClick={this.onClickCoin} />
    })
  }

  render() {
    const { walletCoins } = this.mainStore
    return (
      <div className={'wallet'}>
        <div className="block-title">Your Wallet</div>
        {this.renderCoins(walletCoins)}
      </div>
    )
  }
}
