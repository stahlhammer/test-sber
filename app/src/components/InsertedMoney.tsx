import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { TInjectedMainStore } from '../stores/Main.store'

export interface IInsertedMoneyProps {}

@inject('mainStore')
@observer
export class InsertedMoney extends React.Component<IInsertedMoneyProps> {
  private get injected() {
    return this.props as IInsertedMoneyProps & TInjectedMainStore
  }

  private get mainStore() {
    return this.injected.mainStore
  }

  render() {
    return <div className={'inserted'}>Money inserted: {this.mainStore.insertedSum} ₽</div>
  }
}
