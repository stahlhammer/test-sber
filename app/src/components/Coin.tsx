import * as React from 'react'
import { ICoin } from '../stores/Main.interface'
import { action, observable, runInAction } from 'mobx'
import { Animate } from './utils/Animate'
import { observer } from 'mobx-react'

export interface ICoinProps extends ICoin {
  onClick?: (value: number) => void
}

@observer
export class Coin extends React.PureComponent<ICoinProps> {
  @observable
  private updatedValues: JSX.Element[] = []

  @observable
  private rejectFly?: any

  @action.bound
  private onClick() {
    const { value, amount, onClick } = this.props

    if (amount === 0 || !onClick) {
      return
    } else {
      onClick(value)
    }
  }

  componentWillUpdate(nextProps) {
    if (nextProps.amount !== this.props.amount) {
      const amountDiff = nextProps.amount - this.props.amount
      const isNegativeValue = Math.sign(amountDiff) < 0

      const updatedValue = (
        <div className={`coin__updated ${isNegativeValue ? 'negative' : ''}`}>
          {!isNegativeValue ? '+' : ''}
          {amountDiff}
        </div>
      )

      this.updatedValues = [...this.updatedValues, updatedValue]
    }
  }

  render() {
    const { value, amount } = this.props

    return (
      <div className={'coin'} onClick={this.onClick}>
        <div className={'coin__body'}>{value} ₽</div>
        <Animate className={'coin__amount'}>
          {this.updatedValues}
          <span>{amount}</span>
        </Animate>
      </div>
    )
  }
}
