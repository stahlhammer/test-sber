import * as React from 'react'
import { action, computed } from 'mobx'
import { TInjectedMainStore } from '../stores/Main.store'
import { observer, inject } from 'mobx-react'

export interface IChangeProps {}

@inject('mainStore')
@observer
export class Change extends React.Component<IChangeProps> {
  private get injected() {
    return this.props as IChangeProps & TInjectedMainStore
  }

  private get mainStore() {
    return this.injected.mainStore
  }

  @computed
  private get isDisabled() {
    return this.mainStore.change === 0
  }

  @action.bound
  private onClickChange() {
    this.mainStore.giveChange()
  }

  render() {
    return (
      <div className={'change'}>
        <div className="change__sum">Change: {this.mainStore.change}</div>
        <div
          className={`change__button ${this.isDisabled ? 'disabled' : ''}`}
          onClick={this.onClickChange}
        >
          Pick up change
        </div>
      </div>
    )
  }
}
