import * as React from 'react'
import { observable, runInAction } from 'mobx'
import { observer } from 'mobx-react'

export interface IAnimateProps {
    className: string
}

@observer
export class Animate extends React.Component<IAnimateProps> {
  @observable
  private animateClassName: string = ''

  componentWillReceiveProps() {
    this.animateClassName = 'animate'

    setTimeout(() => {
      runInAction(() => {
        this.animateClassName = ''
      })
    }, 300)
  }

  render() {
      const { className } = this.props
    return <div className={`${className} ${this.animateClassName}`}>{this.props.children}</div>
  }
}
