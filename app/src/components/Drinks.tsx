import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { action } from 'mobx'
import { TInjectedMainStore } from '../stores/Main.store'
import { IDrink, TDrinkName, IImage } from '../stores/Main.interface'
import { Drink } from './Drink'
const tea = require('../assets/lipton.png')
const coffee = require('../assets/nescafe.png')
const cappuccino = require('../assets/cappuccino.png')
const juice = require('../assets/j7.png')

export interface IDrinksProps {}

@inject('mainStore')
@observer
export class Drinks extends React.Component<IDrinksProps> {
  private get injected() {
    return this.props as IDrinksProps & TInjectedMainStore
  }

  private get mainStore() {
    return this.injected.mainStore
  }

  @action.bound
  private onClickDrink(drink: IDrink) {
    const {
      insertedSum,
      moveInsertedMoneyToMachine,
      updateChange,
      giveDrink,
      dispatchSuccess,
    } = this.mainStore

    if (insertedSum > drink.price) {
      updateChange(drink.price)
    }
    moveInsertedMoneyToMachine()
    giveDrink(drink.name)
    dispatchSuccess({
      message: `You have purchased ${drink.name}!`,
    })
  }

  private pickImage(drinkName: TDrinkName) {
    let image: IImage

    switch (drinkName) {
      case 'tea':
        image = tea
        break
      case 'coffee':
        image = coffee
        break
      case 'cappuccino':
        image = cappuccino
        break
      case 'juice':
        image = juice
        break

      default:
        break
    }
    return image
  }

  @action.bound
  private renderDrinks(drinks: IDrink[]) {
    return drinks.map(drink => {
      const image = this.pickImage(drink.name)
      return (
        <Drink
          {...drink}
          image={image}
          key={drink.name}
          onClick={this.onClickDrink}
        />
      )
    })
  }

  render() {
    const {
      drinks,
      hasError,
      errorMessage,
      hasPurchased,
      successMessage,
    } = this.mainStore

    return (
      <div className={'drinks'}>
        <div className={'drinks__wrapper'}>{this.renderDrinks(drinks)}</div>
        {hasError && (
          <div className={'message message--error'}>{errorMessage}</div>
        )}
        {hasPurchased && (
          <div className={'message message--success'}>{successMessage}</div>
        )}
      </div>
    )
  }
}
