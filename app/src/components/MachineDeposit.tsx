import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { action } from 'mobx'
import { TInjectedMainStore } from '../stores/Main.store'
import { Coin } from './Coin'
import { ICoin } from '../stores/Main.interface'

export interface IMachineDepositProps {}

@inject('mainStore')
@observer
export class MachineDeposit extends React.Component<IMachineDepositProps> {
  private get injected() {
    return this.props as IMachineDepositProps & TInjectedMainStore
  }

  private get mainStore() {
    return this.injected.mainStore
  }

  @action.bound
  private renderMachineCoins(machineCoins: ICoin[]) {
    return machineCoins.map(coin => {
      const { amount, value } = coin
      return <Coin value={value} amount={amount} key={value} />
    })
  }

  render() {
    const { machineCoins } = this.mainStore

    return (
      <div className={'machine'}>
        <div className="block-title">Machine Deposit</div>
        {this.renderMachineCoins(machineCoins)}        
      </div>
    )
  }
}
