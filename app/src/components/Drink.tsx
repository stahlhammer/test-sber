import * as React from 'react'
import { IDrink, IError, IImage } from '../stores/Main.interface'
import { inject, observer } from 'mobx-react'
import { TInjectedMainStore } from '../stores/Main.store'
import { action } from 'mobx'

export interface IDrinkProps extends IDrink {
  onClick: (drink: IDrink) => void
  image: IImage
}

@inject('mainStore')
@observer
export class Drink extends React.Component<IDrinkProps> {
  private get injected() {
    return this.props as IDrinkProps & TInjectedMainStore
  }

  private get mainStore() {
    return this.injected.mainStore
  }

  @action.bound
  private onClick() {
    const { insertedSum, dispatchError } = this.mainStore
    const { name, amount, price, onClick } = this.props

    if (insertedSum === 0) {
      const error: IError = {
        message: `You haven't inserted any money`,
      }
      dispatchError(error)
    } else if (insertedSum < price) {
      const error: IError = {
        message: `You haven't inserted enough coins to purchase ${name}`,
      }
      dispatchError(error)
    } else {
      onClick({ name, price, amount })
    }
  }

  render() {
    const { name, price, amount, image } = this.props
    return (
      <div className={'drink'} onClick={this.onClick}>
        <div className="drink__logo">
          <div
            className="drink__img"
            style={{ backgroundImage: `url(${image.default})` }}
          ></div>
        </div>
        <div className="drink__description">
          <div className={'drink__name'}>{name}</div>
          <div className={'drink__price'}>{price} ₽</div>
        </div>
        <div className={'drink__amount'}>{amount}</div>
      </div>
    )
  }
}
