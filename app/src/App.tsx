import * as React from 'react'
import { observer, inject } from 'mobx-react'
import Wallet from './components/Wallet'
import { MachineDeposit } from './components/MachineDeposit'
import { InsertedMoney } from './components/InsertedMoney'
import { Drinks } from './components/Drinks'
import { Change } from './components/Change'

@inject('mainStore')
@observer
export default class App extends React.Component<{}> {
  render() {
    return (
      <div className={'main-wrapper'}>
        <MachineDeposit />
        <div className={'app-screen'}>
          <Drinks />
          <InsertedMoney />
          <Change />
        </div>
        <Wallet />
      </div>
    )
  }
}
