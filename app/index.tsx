import * as React from 'react'
import * as ReactDOM from 'react-dom'
import App from './src/App'
import { Provider } from 'mobx-react'
import './styles.scss'
import { MainStore } from './src/stores/Main.store'

const mainStore = new MainStore()

ReactDOM.render(
  <Provider mainStore={mainStore}>
    <App />
  </Provider>,
  document.getElementById('root')
)
